import React, {Component} from "react";
import {Field, reduxForm} from "redux-form";

class Payment extends Component {

    

    renderTypeField(field){
        return (
            <div className="form-group">
                <input
                    className="form-control"
                    type="radio"
                    {...field.radio}
                />
            </div>
        )
    };

    renderCodeField(field){
        return (
            <div className="form-group">
                <input
                    className="form-control"
                    type="text"
                    {...field.input}
                />
                <div className="text-help">
                    {field.meta.touched ?  field.meta.error : ''}
                </div>
            </div>
        )
    };

    renderAmountField(field){
        return (
            <div className="form-group">
                <input
                    className="form-control"
                    disabled="disabled"
                    type="text"
                    {...field.input}
                />
            </div>
        )
    };

    onSubmit(values){
        console.log(values);
        this.props.history.push('/process');
    }


    render() {
        const { handleSubmit } = this.props;

        return (
            <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                <div>
                    <label>Card Type</label>
                    <div>
                        <label><Field  name = "type" component={this.renderTypeField}  value="visa"/> <img src="./public/visa.png" width="50" height="30"/> </label>
                        <label><Field name = "type" component={this.renderTypeField}  value="master"/> <img src="./public/mastercard.png" width="50" height="30"/></label>
                    </div>
                </div>
                <div>
                    <label>Card Security Code</label>
                    <div>
                        <Field name="code" component={this.renderCodeField}  />
                    </div>
                </div>
                <div>
                    <label>Amount</label>
                    <div>
                        <Field name="amount" component={this.renderAmountField}  />
                    </div>
                </div>
                <button type="submit" className="btn btn-primary">Submit</button>
            </form>
        );
    }
}

function  validate(values) {
    const errors = {};
    if(!values.code){
        errors.code = "Enter a security code!";
    }
    return errors;
}

export default reduxForm({
    validate,
    form: 'PaymentForm'
})(Payment);
